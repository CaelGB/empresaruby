require 'faker'
class Funcionario
    attr_accessor :first_name, :surname, :age, :salary, :address, :dependents, :sector, :register


    def initialize 
        @first_name = Faker::Name.first_name #Nome
        @surname =  Faker::Name.last_name#Sobrenome
        @age = Faker::Number.between(from: 18, to: 65).floor #Idade
        @salary = Faker::Number.between(from: 1080, to: 10000) #Salario
        @address = Faker::Address.full_address #Endereço
        @dependents = [] #Vetor dos dependentes
        @sector = Faker::Address.city #setor
        @register = Faker::Number.number #Matricula
    end

    def nome_completo 
        "#{first_name} #{surname}"
    end

    def adicionar_dependente dependente 
        dependents << dependente
    end

    def n_dependentes
        dependents.length
    end
end