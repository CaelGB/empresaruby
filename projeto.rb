require 'faker'

class Projeto 
    attr_accessor :name, :client, :funds, :department

    def initialize departamento
        @name = Faker::Space.constellation
        @client = Faker::Space.company
        @funds = Faker::Number.between(from: 1000000, to: 1000000000)
        @department = departamento
    end
end