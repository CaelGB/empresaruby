require 'faker'

class Departamento
    attr_accessor :employees, :name, :manager

    def initialize funcionarios, gerente
        @employees = funcionarios
        @name = Faker::Address.city
        @manager = gerente
    end

    def adicionar_funcionario funcionario
        employees << funcionario
    end

    def gastos_funcionario
        expenses = 0
        employees.each do |employee|
            expenses += employee.salary
        end
        expenses
    end

    def num_funcionarios
        employees.length
    end

    def adiciona_gerente funcionario
        manager = funcionario
    end
end