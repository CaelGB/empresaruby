require './armazenamento.rb'
require './departamento.rb'
require './funcionario.rb'
require './dependente.rb'
require './projeto.rb'

armazenamento = Armazenamento.new

#Gerando Funcionarios

armazenamento.funcionarios = Array.new 20

for i in 0 .. (armazenamento.funcionarios.length - 1) do
    armazenamento.funcionarios[i] = Funcionario.new
end

#Gerando Dependentes

armazenamento.dependentes = Array.new 30

for i in 0 .. (armazenamento.dependentes.length - 1) do
    armazenamento.dependentes[i] = Dependente.new
end

armazenamento.add_depedents 2

#Gerando Departamentos

armazenamento.departamentos = armazenamento.generate_department_staff 4

#Gera projetos
armazenamento.projetos = Array.new armazenamento.departamentos.length

armazenamento.departamentos.each do |departamento|
    armazenamento.projetos << Projeto.new(departamento)
end