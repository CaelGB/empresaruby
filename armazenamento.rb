class Armazenamento
    attr_accessor :funcionarios, :dependentes, :departamentos, :projetos
    @@funcionarios = []
    @@dependentes = []
    @@departamentos = []
    @@projetos = []


    #A forma que essas funções são feitas pode levar a funcionarios e dependentes sobrando
    def generate_department_staff n_deps
        department_staff = Array.new n_deps
        a = 0
        b = (self.funcionarios.length/n_deps).floor
        for i in 0..(n_deps - 1) do
            department_staff[i] = {manager: self.funcionarios[a], staff: self.funcionarios[a..b]}
        end

        department_staff
    end

    def add_depedents n_dependents
        a = 0
        b = n_dependents
        self.funcionarios.each do |employee|
            if b >= self.funcionarios.length
                break
            end
            employee.dependents = self.dependentes[a..b]
            a = b+1
            b += n_dependents
        end
    end
end