require 'faker'

class Dependente
    attr_accessor :name, :age, :relation

    def initialize
        @name = Faker::Name.first_name
        @age = Faker::Number.between(from: 18, to: 65).floor
        @relation = Faker::Relationship.familial
    end
end